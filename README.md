# OpenRAP
Repository for Content related to Open RAP (Resource Access Point)

Note - for Ubuntu installation, this directory must be cloned into /opt/opencdn/ folder of your system with the user 'admin'

# Installing on Ubuntu
## Step 1 - Install the basic dependencies
### Golang
#### Install Go. Set your GOPATH. Then install the following packages using `go get -u`

* github.com/golang/dep/cmd/dep
* github.com/gorilla/mux
* github.com/blevesearch/bleve
* github.com/blevesearch/bleve-mapping-ui
* github.com/blevesearch/snowballstem
* github.com/couchbase/moss
* github.com/syndtr/goleveldb/leveldb
* golang.org/x/text/unicode/norm
* github.com/willf/bitset
* github.com/mohae/deepcopy

In case there are any missing packages, just check the error message for the same and install them using `go get -u`

### Node.js
#### Install Node.js and NPM. This would be required for the next step

### MySQL
#### Install and start mysql with user root and password root. The database will be created and populated in one of the next steps.

## Step 2 - Parts of the OpenRAP system
### devmgmtV2
### dbsdk
### dbsdk2
### filesdk
### searchsdk
### appServer
#### First create an administrator user on your Ubuntu system named `admin`. Next, log in to admin, and clone this repository in `/opt/opencdn/` folder of your system. Next, `cd` into each of the above compoment folders and run `npm install` separately. If you run into an error related to permission or paths, try to fix them by performing npm install for specific dependencies.

### devmgmtui
#### This is the actual service that runs the backend. 
* `cd` into the directory.
* run `npm install`
* run `npm run build`
* copy the contents of the newly created build folder into rootfs_overlay/var/www/html/admin i.e.
`cp -r devmgmtui/build/* rootfs_overlay/var/www/html/admin/`

### searchServer
#### Build the Go based search server using `go build` inside this directory

## Step 3 - Update the host files
You need to update the files in `/etc` with the files provided in `rootfs_overlay` folder in this repository. Some parts are optional. Need more info in this step.

## Step 4 - Running the portal
To run the portal -
* Start the searchServer - `./searchServer` - this executable should've been created in the last part of Step 2
* Run `node index.js` in appServer, searchsdk, filesdk and devmgmtV2 folders
* Run `npm start` in devmgmtui folder
If everything is okay `http://localhost:3000/` should be opened now in your default browser. Login using username `root` with password `root` to access the admin portal.