"use strict"

let testIndex = (req, res) => {
  let responseStructure = {
    retrieveSuccessful : false,
    msg : '',
    userList : []
  };
  responseStructure.retrieveSuccessful = true;
  responseStructure.msg = "Successfully retrieved user list";
  return res.status(200).json(responseStructure);
}

module.exports = {
  testIndex
}