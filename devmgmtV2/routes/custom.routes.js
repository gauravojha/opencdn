"use strict";

let {
    testIndex
} = require("../controllers/custom.controller.js");

module.exports = app => {
    app.get("/", testIndex);
}
