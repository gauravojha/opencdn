/*
    Template controller files
    Your program logic goes here .
    The functions contains the logic and should be exported to be used in routes
*/

let testPluginFunc = (req, res) => {
    res.send("I just built this test plugin for OpenRAP");
}

module.exports = {
    testPluginFunc
}
